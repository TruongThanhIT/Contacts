﻿using System;
using System.Collections.Generic;
using Contacts.iOS;
using Contacts.Models;
using Foundation;

[assembly: Xamarin.Forms.Dependency(typeof(GetContacts_iOS))]
namespace Contacts.iOS
{
    public class GetContacts_iOS : IGetContacts
    {
        List<Contact> IGetContacts.GetContact()
        {
            var keysToFetch = new[] { CNContactKey.GivenName, CNContactKey.FamilyName, CNContactKey.OrganizationName };
            NSError error;
            //var containerId = new CNContactStore().DefaultContainerIdentifier;
            // using the container id of null to get all containers.
            // If you want to get contacts for only a single container type, you can specify that here
            var contactList = new List<CNContact>();

            using (var store = new CNContactStore())
            {
                var allContainers = store.GetContainers(null, out error);
                foreach (var container in allContainers)
                {
                    try
                    {
                        using (var predicate = CNContact.GetPredicateForContactsInContainer(container.Identifier))
                        {
                            var containerResults = store.GetUnifiedContacts(predicate, keysToFetch, out error);
                            contactList.AddRange(containerResults);
                        }
                    }
                    catch (Exception)
                    {
                        // ignore missed contacts from errors
                    }
                }
            }
            var contacts = new List<Contact>();

            foreach (var item in contactList)
            {
                if (item != null)
                {
                    contacts.Add(new Contact
                    {
                        Id = item.Identifier,
                        FirstName = item.GivenName,
                        LastName = item.FamilyName,
                        CompanyName = item.OrganizationName
                    });
                }
            }
            return contacts;
        }

        ContactDetail IGetContacts.GetContactDetailByID(string id)
        {
            var contactDetail = new ContactDetail();
            var keysToFetch = new[] { CNContactKey.GivenName, CNContactKey.FamilyName, CNContactKey.OrganizationName, CNContactKey.PhoneNumbers, CNContactKey.EmailAddresses, CNContactKey.ImageData, CNContactKey.ImageDataAvailable };
            NSError error;
            using (var store = new CNContactStore())
            {
                try
                {
                    var contact = store.GetUnifiedContact(id, keysToFetch, out error);
                    if (contact != null)
                    {
                        contactDetail.CompanyName = contact.OrganizationName;
                        contactDetail.FirstName = contact.GivenName;
                        contactDetail.LastName = contact.FamilyName;
                        if (contact.ImageDataAvailable)
                        {
                            contactDetail.SetImage(contact.ImageData.AsStream());
                        }

                        var numbers = contact.PhoneNumbers;
                        contactDetail.Phones = new System.Collections.ObjectModel.ObservableCollection<string>();
                        if (numbers != null)
                        {
                            foreach (var number in numbers)
                            {
                                contactDetail.Phones.Add(number.Value.StringValue);
                            }
                        }

                        var emails = contact.EmailAddresses;
                        contactDetail.Emails = new System.Collections.ObjectModel.ObservableCollection<string>();
                        if (emails != null)
                        {
                            foreach (var email in emails)
                            {
                                contactDetail.Emails.Add(email.Value.ToString());
                            }
                        }
                    }
                }
                catch
                {
                }
            }
            return contactDetail;
        }
    }
}