﻿using Prism.Mvvm;

namespace Contacts.Models
{
    public class PhoneModel : BindableBase
    {
        private string _tittle;
        private string _phoneNumber;

        public PhoneModel()
        {
        }

        public PhoneModel(string phone, string tittle)
        {
            PhoneNumber = phone;
            Tittle = tittle;
        }

        public string Tittle { get => _tittle; set => SetProperty(ref _tittle, value); }

        public string PhoneNumber { get => _phoneNumber; set => SetProperty(ref _phoneNumber, value); }
    }
}
