﻿using System.IO;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace Contacts.Models
{
    public class ContactDetail : Contact
    {
        private ImageSource _image;
        private ObservableCollection<string> _phones;
        private ObservableCollection<string> _emails;
        public static string IMAGE_DEFAUL = "Contacts.Resources.user.png";

        public ImageSource Image { get => _image; set => SetProperty(ref _image, value); }
        public ObservableCollection<string> Phones { get => _phones; set => SetProperty(ref _phones, value); }
        public ObservableCollection<string> Emails { get => _emails; set => SetProperty(ref _emails, value); }

        public ContactDetail()
        {
        }

        public void SetImage(Stream stream)
        {
            if(stream == null) {
                _image = ImageSource.FromResource(IMAGE_DEFAUL);
            } else {
                _image = ImageSource.FromStream(() => stream);
            }
        }
    }
}
