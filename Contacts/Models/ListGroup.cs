﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Contacts.Models
{
    public class ListGroup<K, T> : ObservableCollection<T>
    {
        public K GroupKey { get; }
        public ListGroup(K groupKey, IEnumerable<T> items)
        {
            GroupKey = groupKey;
            foreach (var item in items)
                this.Items.Add(item);
        }
    }
}

