﻿using Prism.Mvvm;

namespace Contacts.Models
{
    public class Contact : BindableBase
    {
        private string _id;
        private string _firstName;
        private string _lastName;
        private string _companyName;

        public string Id { get => _id; set => SetProperty(ref _id, value); }

        public string FirstName { 
            get => _firstName; 
            set
            {
                SetProperty(ref _firstName, value);
                RaisePropertyChanged(nameof(DisplayName));
            }
        }

        public string LastName
        {
            get => _lastName;
            set
            {
                SetProperty(ref _lastName, value);
                RaisePropertyChanged(nameof(DisplayName));
            }
        }

        public string CompanyName { get => _companyName; set => SetProperty(ref _companyName, value); }


        public string DisplayName => FirstName + " " + LastName;

        public string NameSort
        {
            get
            {
                if (string.IsNullOrWhiteSpace(FirstName) || FirstName.Length == 0)
                    return "?";

                return FirstName[0].ToString().ToUpper();
            }
        }
    }
}
