﻿using Contacts.Enums;

namespace Contacts.EventArgs
{
    public class MailCellEventArgs : System.EventArgs
    {
        public string MailAddress { get; private set; }
        public ContactInfoEventEnum ContactInfoEventEnum { get; private set; }

        public MailCellEventArgs(ContactInfoEventEnum events, string mailAddress)
        {
            ContactInfoEventEnum = events;
            MailAddress = mailAddress;
        }
    }
}
