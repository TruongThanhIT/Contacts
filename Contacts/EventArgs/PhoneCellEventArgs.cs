﻿using Contacts.Enums;

namespace Contacts.EventArgs
{
    public class PhoneCellEventArgs : System.EventArgs
    {
        public string PhoneNumber { get; private set; }
        public ContactInfoEventEnum CellEvent { get; private set;  }

        public PhoneCellEventArgs(ContactInfoEventEnum events, string number)
        {
            CellEvent = events;
            PhoneNumber = number;
        }
    }
}
