﻿using System;

using Xamarin.Forms;

namespace Contacts.View
{
    public class MyView : ContentView
    {
        public MyView()
        {
            Content = new Label { Text = "Hello ContentView" };
        }
    }
}

