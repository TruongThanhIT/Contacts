﻿using System;
using Contacts.Enums;
using Contacts.EventArgs;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Contacts.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PhonesViewCell : ContentView
    {
        public event EventHandler<PhoneCellEventArgs> OnPhoneCellTap;
        public PhonesViewCell()
        {
            InitializeComponent();
            imgMessage.BindingContext = this;
            imgCall.BindingContext = this;
            imgMessage.Source = ImageSource.FromResource("Contacts.Resources.messageAnd.png");
            imgCall.Source = ImageSource.FromResource("Contacts.Resources.callAnd.png");
            try
            {
                TapGestureRecognizer msgTap = new TapGestureRecognizer();
                TapGestureRecognizer callTap = new TapGestureRecognizer();
                msgTap.Tapped += MsgTap_Tapped;
                callTap.Tapped += CallTap_Tapped;

                imgMessage.GestureRecognizers.Add(msgTap);
                imgCall.GestureRecognizers.Add(callTap);
            }
            catch (Exception e)
            {
                e.Message.ToString();
            }
        }

        private void CallTap_Tapped(object sender, System.EventArgs e)
        {
            OnPhoneCellTap?.Invoke(this, new PhoneCellEventArgs(ContactInfoEventEnum.Call, entPhone.Text));
        }

        void MsgTap_Tapped(object sender, System.EventArgs e)
        {
            OnPhoneCellTap?.Invoke(this, new PhoneCellEventArgs(ContactInfoEventEnum.Message, entPhone.Text));
        }
    }
}
