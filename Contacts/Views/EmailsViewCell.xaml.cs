﻿using System;
using Contacts.Enums;
using Contacts.EventArgs;
using Xamarin.Forms;

namespace Contacts.Views
{
    public partial class EmailsViewCell : ContentView
    {
        public event EventHandler<MailCellEventArgs> OnMailCellTap;

        public EmailsViewCell()
        {
            InitializeComponent();
            imgMail.BindingContext = this;
            imgMail.Source = ImageSource.FromResource("Contacts.Resources.mailAnd.png");

            //imgMail.Source = Device.OnPlatform(
            //iOS: ImageSource.FromFile("Contacts/Resources/mailiOS.png"),
            //Android: ImageSource.FromFile("Contacts/Resources/mailAnd.png"));
            TapGestureRecognizer mailTap = new TapGestureRecognizer();
            mailTap.Tapped += MailTap_Tapped;
            imgMail.GestureRecognizers.Add(mailTap);
        }

        private void MailTap_Tapped(object sender, System.EventArgs e)
        {
            OnMailCellTap?.Invoke(this, new MailCellEventArgs(ContactInfoEventEnum.Mail, entMail.Text));
        }
}
}
