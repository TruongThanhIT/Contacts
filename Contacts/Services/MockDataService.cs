﻿using System.Collections.ObjectModel;
using Xamarin.Forms;

using Contacts.Models;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Threading.Tasks;

namespace Contacts.Services
{
    public class MockDataService:IDataService
    {
        public ObservableCollection<Contact> list { get; set;}

        public async Task<ObservableCollection<Contact>> GetContacts()
        {
            if (await CheckPermission())
            {
                var addressBook = DependencyService.Get<IGetContacts>();
                if (addressBook != null)
                {
                    var allAddress = addressBook.GetContact();
                    list = new ObservableCollection<Contact>(allAddress);
                }
                return list;
            }
            else
            {
                return list = new ObservableCollection<Contact>();   
            }

        }

        public ContactDetail GetContact(string id)
        {
            var addressBook = DependencyService.Get<IGetContacts>();
            return addressBook.GetContactDetailByID(id);
        }

        public async Task<bool> CheckPermission()
        {
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Contacts);
                if (status == PermissionStatus.Granted)
                    return true;
                while(status != PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Contacts);
                    status = results[Permission.Contacts];
                    if (status == PermissionStatus.Granted)
                        return true;
                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }
    }
}
