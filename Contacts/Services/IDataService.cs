﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Contacts.Models;

namespace Contacts.Services
{
    public interface IDataService
    {
        Task<ObservableCollection<Contact>> GetContacts();
        ContactDetail GetContact(string id);
    }
}
