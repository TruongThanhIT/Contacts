﻿using System;
using System.ComponentModel.IContainer;
namespace Contacts
{
    public class AppSetup
    {
        public IContainer CreateContainer()
        {
            var containerBuilder = new ContainerBuilder();
            RegisterDependencies(containerBuilder);
            return containerBuilder.Build();
        }
    }
}
