﻿using Contacts.Services;
using Contacts.Views;
using Microsoft.Practices.Unity;
using Prism.Unity;
using Xamarin.Forms;

namespace Contacts
{
    public partial class App
    {
        public App()
        {
            

        }
        protected override void OnInitialized()
        {
            InitializeComponent();

            NavigationService.NavigateAsync("Navigation/Contact");
        }
        protected override void RegisterTypes()
        {
            Container.RegisterType(typeof(IDataService), typeof(MockDataService), new ContainerControlledLifetimeManager());
            Container.RegisterTypeForNavigation<NavigationPage>("Navigation");
            Container.RegisterTypeForNavigation<ContactListPage>("Contact");
            Container.RegisterTypeForNavigation<DetailPage>("Detail");
        }
    }
}
