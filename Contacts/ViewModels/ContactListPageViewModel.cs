﻿using System.Linq;
using System.Windows.Input;
using System.Collections.ObjectModel;

using Prism.Commands;
using Prism.Navigation;

using Contacts.Models;
using Contacts.Services;

namespace Contacts.ViewModels
{

    public class ContactListPageViewModel : ViewModelBase
    {
        private IDataService _dataService;
        private ObservableCollection<Contact> _contacts;
        private ObservableCollection<ListGroup<string, Contact>> _contactGrouped;

        public ICommand NewCommand { get; set; }

        public ContactListPageViewModel(INavigationService navigationService, IDataService dataService) : base(navigationService)
        {
            _dataService = dataService;
            NewCommand = new DelegateCommand<Contact>(ExcuteMethod);
        }

        private void ExcuteMethod(Contact contact)
        {
            _navigationService.NavigateAsync($"Detail?id={contact.Id}");
        }

        public ObservableCollection<Contact> Contacts
        {
            get => _contacts;
            set => SetProperty(ref _contacts, value);
        }

        public ObservableCollection<ListGroup<string, Contact>> ContactGrouped
        {
            get => _contactGrouped;
            set => SetProperty(ref _contactGrouped, value);
        }

        public override async void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (Contacts != null && Contacts.Any())
                return;
            Contacts = await _dataService.GetContacts();
            var sorted = from contact in Contacts
                         orderby contact.FirstName
                         group contact by contact.NameSort into contactGroup
                         select new ListGroup<string, Contact>(contactGroup.Key, contactGroup);
            ContactGrouped = new ObservableCollection<ListGroup<string, Contact>>(sorted);
        }
    }
}
