﻿using System.Collections.ObjectModel;
using Xamarin.Forms;

using Prism.Navigation;
using Plugin.Messaging;

using Contacts.Enums;
using Contacts.Models;
using Contacts.Services;
using Contacts.EventArgs;

namespace Contacts.ViewModels
{
    public class DetailPageViewModel : ViewModelBase
    {
        private string _name;
        private ImageSource _image;
        private string _companyName;
        private ContactDetail _contact;
        private readonly IDataService _dataService;
        private ObservableCollection<string> _phones;
        private ObservableCollection<string> _emails;

        public Command OnMailCellTapCommand { get; private set; }
        public Command OnPhoneCellTapCommand { get; private set; }

        public DetailPageViewModel(INavigationService navigationService, IDataService dataService) : base(navigationService)
        {
            _dataService = dataService;
            InitCommand();
        }

        void InitCommand()
        {
            OnMailCellTapCommand = new Command<MailCellEventArgs>(MailCellTappedExcute);
            OnPhoneCellTapCommand = new Command<PhoneCellEventArgs>(PhoneCellTappedExcute);
        }

        public string Name
        {
            get => _name; set => SetProperty(ref _name, value);
        }

        public ImageSource Image
        {
            get => _image; set => SetProperty(ref _image, value, "Image");
        }

        public string CompanyName
        {
            get => _companyName; set => SetProperty(ref _companyName, value);
        }

        public ContactDetail Contact
        {
            get => _contact;
            set => SetProperty(ref _contact, value);
        }

        public ObservableCollection<string> Phones
        {
            get => _phones; set => SetProperty(ref _phones, value);
        }

        public ObservableCollection<string> Emails
        {
            get => _emails; set => SetProperty(ref _emails, value);
        }

        private void MailCellTappedExcute(MailCellEventArgs args)
        {
            var emailMessenger = CrossMessaging.Current.EmailMessenger;
            if (emailMessenger.CanSendEmail)
            {
                emailMessenger.SendEmail(args.MailAddress);
            }
        }

        private void PhoneCellTappedExcute(PhoneCellEventArgs args)
        {
            if (args.CellEvent.Equals(ContactInfoEventEnum.Message))
            {
                var smsMessenger = CrossMessaging.Current.SmsMessenger;
                if (smsMessenger.CanSendSms)
                {
                    smsMessenger.SendSms(args.PhoneNumber, "");
                }
            }
            else
            {
                var phoneCallTask = CrossMessaging.Current.PhoneDialer;
                if (phoneCallTask.CanMakePhoneCall)
                {
                    phoneCallTask.MakePhoneCall(args.PhoneNumber, "");
                }
            }
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (parameters.ContainsKey("id"))
            {
                var id = parameters["id"];
                Contact = _dataService.GetContact(id.ToString());
                Name = Contact.FirstName;
                CompanyName = Contact.CompanyName;
                Phones = Contact.Phones;
                Emails = Contact.Emails;
                Image = Contact.Image;
            }
        }
    }
}
