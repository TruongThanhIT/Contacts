﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contacts.Models;

namespace Contacts
{
    public interface IGetContacts
    {
        List<Contact> GetContact();
        ContactDetail GetContactDetailByID(string id);
    }
}
