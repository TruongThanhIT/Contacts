﻿using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Java.Lang;
using Android.Provider;
using Android.Content;
using Android;
using Android.OS;
using Android.App;
using Android.Content.PM;
using Android.Support.V4.App;
using static Android.Provider.ContactsContract.RawContacts;
using static Android.Provider.ContactsContract.CommonDataKinds;

using Contacts.Droid;
using Contacts.Models;

[assembly: Xamarin.Forms.Dependency(typeof(GetContacts_Android))]
namespace Contacts.Droid
{
    public class GetContacts_Android : IGetContacts
    {
        private int ResultValue = 0;
        private ContentResolver contentProvider = Application.Context.ContentResolver;

        List<Contact> IGetContacts.GetContact()
        {
            var phoneContacts = new List<Contact>();
            var uri = Phone.ContentUri;
            List<string> names = new List<string>();
            using (var phones = Application.Context.ContentResolver.Query(uri, null, null, null, null))
            {
                if (phones != null)
                {
                    while (phones.MoveToNext())
                    {
                        var contact = new Contact();

                        string name = phones.GetString(phones.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.DisplayName));

                        string id = phones.GetString(phones.GetColumnIndex(InterfaceConsts.ContactId));

                        var URI_ORGNIZATION = ContactsContract.Data.ContentUri;
                        string SELECTION_ORGNIZATION = ContactsContract.Data.InterfaceConsts.ContactId + " = ? AND " + ContactsContract.Data.InterfaceConsts.Mimetype + " = ?";
                        string[] SELECTION_ARRAY_ORGNIZATION = new string[] { id, Organization.ContentItemType };
                        var currOrg = contentProvider.Query(URI_ORGNIZATION, null, SELECTION_ORGNIZATION, SELECTION_ARRAY_ORGNIZATION, null);
                        if (currOrg.MoveToNext())
                        {
                            string companyName = currOrg.GetString(currOrg.GetColumnIndex(Organization.Company));
                            contact.CompanyName = companyName;
                        }

                        string[] words = name.Split(' ');
                        contact.Id = id;
                        contact.FirstName = words[0];
                        if (words.Length > 1)
                            contact.LastName = words[1];
                        else
                            contact.LastName = ""; //no last name

                        var flag = false;
                        foreach (string n in names)
                        {
                            if (n == name)
                            {
                                flag = true;
                                break;
                            }
                        }
                        if (!flag)
                        {
                            phoneContacts.Add(contact);
                            names.Add(name);
                        }
                    }
                    phones.Close();
                }
            }

            return phoneContacts;
        }

        //public int CheckPermission()
        //{
        //    if ((int)Build.VERSION.SdkInt < 23)
        //    {
        //        return 1;
        //    }
        //    if (Xamarin.Forms.Forms.Context.CheckSelfPermission(Manifest.Permission.ReadContacts) != Permission.Granted)
        //        ActivityCompat.RequestPermissions((Activity)Xamarin.Forms.Forms.Context, new System.String[] { Manifest.Permission.ReadContacts }, ResultValue);
        //    return 0;
        //}

        ContactDetail IGetContacts.GetContactDetailByID(string id)
        {

            ContactDetail contactDetail = new ContactDetail();

            try
            {
                var uri = ContentUris.WithAppendedId(ContactsContract.Contacts.ContentUri, Long.ValueOf(id).LongValue());
                Stream inputStream = ContactsContract.Contacts.OpenContactPhotoInputStream(contentProvider, uri);

                contactDetail.SetImage(inputStream);

                var cursorPhone = contentProvider.Query(Phone.ContentUri,
                                                        new string[] { ContactsContract.Contacts.InterfaceConsts.DisplayName },
                                                        InterfaceConsts.ContactId + " = ?", new string[] { id },
                   null);
                if (cursorPhone.MoveToFirst())
                {
                    string fullName = cursorPhone.GetString(cursorPhone.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.DisplayName));
                    string[] words = fullName.Split(' ');
                    contactDetail.FirstName = words[0];
                    if (fullName.Length > 1)
                        contactDetail.LastName = words[1];
                    else
                        contactDetail.LastName = ""; //no last name
                }
                var URI_ORGNIZATION = ContactsContract.Data.ContentUri;
                string SELECTION_ORGNIZATION = ContactsContract.Data.InterfaceConsts.ContactId + " = ? AND " + ContactsContract.Data.InterfaceConsts.Mimetype + " = ?";
                string[] SELECTION_ARRAY_ORGNIZATION = new string[] { id, Organization.ContentItemType };
                var currOrg = contentProvider.Query(URI_ORGNIZATION, null, SELECTION_ORGNIZATION, SELECTION_ARRAY_ORGNIZATION, null);
                if (currOrg.MoveToNext())
                {
                    string companyName = currOrg.GetString(currOrg.GetColumnIndex(Organization.Company));
                    contactDetail.CompanyName = companyName;
                }
            }
            catch (IOException)
            {

            }
            contactDetail.Phones = new ObservableCollection<string>(retrieveContactNumber(id));
            contactDetail.Emails = new ObservableCollection<string>(retrieveContactEmail(id));

            return contactDetail;
        }

        private List<string> retrieveContactNumber(string contactId)
        {

            List<string> contactNumber = new List<string>();
            // Using the contact ID now we will get contact phone number
            var cursorPhone = contentProvider.Query(Phone.ContentUri,
                                                    new string[] { Phone.Number },
                                                    InterfaceConsts.ContactId + " = ?", new string[] { contactId },
                    null);
            if (cursorPhone.Count > 0)
            {
                while (cursorPhone.MoveToNext())
                {
                    contactNumber.Add(cursorPhone.GetString(cursorPhone.GetColumnIndex(Phone.Number)));
                }
            }
            cursorPhone.Close();
            return contactNumber;
        }

        private List<string> retrieveContactEmail(string contactId)
        {

            List<string> contactEmail = new List<string>();

            var cursorEmail = contentProvider.Query(Email.ContentUri,
                                                    new string[] { Email.Address },
                                                    InterfaceConsts.ContactId + " = ?", new string[] { contactId }, null);
            if (cursorEmail.Count > 0)
            {
                while (cursorEmail.MoveToNext())
                {
                    contactEmail.Add(cursorEmail.GetString(cursorEmail.GetColumnIndex(Email.Address)));
                }

            }
            cursorEmail.Close();
            return contactEmail;
        }
    }
}
